#ifdef __linux__
#include <PipedProcess/Pipe.hpp>

Pipe::Pipe() {
	assert(::pipe(this->pipe) != -1);
}

bool Pipe::closePipe() const noexcept {
	return (close(this->pipe[Pipe::Read]) == 0) && (close(this->pipe[Pipe::Write]) == 0);
}

bool Pipe::closeReader() const noexcept {
	return close(this->pipe[Pipe::Read]) == 0;
}

bool Pipe::closeWriter() const noexcept {
	return close(this->pipe[Pipe::Write]) == 0;
}

int Pipe::write(const char *buffer, int size) const {
	return ::write(this->pipe[Pipe::Write], buffer, size);
}

int Pipe::read(char *buffer, int size) const {
	int len;
	while((len = ::read(this->pipe[Pipe::Read], buffer, size)) == 0);
	if(len >= 0)
		buffer[len] = '\0';
	return len;
}

#endif /* __linux__ */