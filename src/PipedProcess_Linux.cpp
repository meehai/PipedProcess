#ifdef __linux__
#include <PipedProcess/PipedProcess.hpp>

PipedProcess::PipedProcess() {}

bool PipedProcess::setReadPipeBlocking(bool blocking) {
	return fcntl(this->readPipe.getReader(), F_SETFL, blocking ? this->readFlags : this->readFlags | O_NONBLOCK) != -1;
}

bool PipedProcess::nonBlockingReturn() {
	return errno == EAGAIN || errno == EWOULDBLOCK;
}

void PipedProcess::startProcess() {
	assert(this->arguments.argc > 0 && this->arguments.argv != nullptr);
	/* This cannot be called anymore, because the arguments are freed later. */
	this->printSpawning();

	if(!fork()) {
		/* Duplicate the stdin and stdout using the 2 pipes */
		assert(dup2(this->writePipe.getReader(), STDIN_FILENO) != -1);
		assert(dup2(this->readPipe.getWriter(), STDOUT_FILENO) != -1);

		/* Actually close the pipes on the forked child */
		this->writePipe.closePipe();
		this->readPipe.closePipe();

		/* If the parent dies, send SIGHUP to the new process */
		assert(prctl(PR_SET_PDEATHSIG, SIGHUP) != -1);

		/* Second process will have the writing pipe opened from the parent process. */
		close(3);

		/* Execute the new process' code */
		assert(execve(this->arguments.argv[0], this->arguments.argv, nullptr) != -1);
		exit(100);
	}
	else {
		/* Parent only needs the writer end of writer pipe and reader end of read pipe */
		this->writePipe.closeReader();
		this->readPipe.closeWriter();
		assert((this->readFlags = fcntl(this->readPipe.getReader(), F_GETFL, 0)) != -1);

		this->arguments.argv = nullptr;
		this->arguments.argc = 0;
		this->started = true;
		this->startCommunication();
	}
}

#endif /* __linux__ */