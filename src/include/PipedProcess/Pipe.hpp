/* Pipe.hpp - Class that defines a pipe in Linux's terms. Basic open/read/close methods are provided. */
#ifndef PIPE_HPP
#define PIPE_HPP

#include <assert.h>
#include <cstring>
#include <ctype.h>
#ifdef __linux__
#include <unistd.h>
#elif _WIN32
#include <Windows.h>
#endif /* __linux__ */

#ifdef __linux__
	typedef int PipeHandle;
#elif _WIN32
	typedef HANDLE PipeHandle;
#endif

class Pipe {
public:
	Pipe();
	Pipe(int, int);
	bool closePipe() const noexcept; /* Closes the pipe's two file descriptors */
	bool closeReader() const noexcept;
	bool closeWriter() const noexcept;
	int write(const char *, int) const; /* Writes data to the write end of the pipe */
	int read(char *, int) const; /* Reads data from the read end of the pipe */
	int readNoSpecialCharacters(char *, int) const; /* Calls read and then strips '\n' or ' ' from beginning and end. */
	PipeHandle getReader() const;
	PipeHandle getWriter() const;
	~Pipe();

	const static int Read = 0;
	const static int Write = 1;
	
private:
	PipeHandle pipe[2];
#ifdef _WIN32
	SECURITY_ATTRIBUTES securityAttributes;
#endif
};

#endif /* PIPE_HPP */