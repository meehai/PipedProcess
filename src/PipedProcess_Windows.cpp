#ifdef _WIN32
#include <PipedProcess/PipedProcess.hpp>

HANDLE PipedProcess::handlerJob = CreateJobObject(nullptr, nullptr);
JOBOBJECT_EXTENDED_LIMIT_INFORMATION PipedProcess::jeli = {0};

PipedProcess::PipedProcess() {
	assert(PipedProcess::handlerJob);
    PipedProcess::jeli.BasicLimitInformation.LimitFlags = JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE;
    assert(SetInformationJobObject(PipedProcess::handlerJob, JobObjectExtendedLimitInformation, &PipedProcess::jeli,
    	sizeof(JOBOBJECT_EXTENDED_LIMIT_INFORMATION)));
}

bool PipedProcess::setReadPipeBlocking(bool blocking) {
	DWORD mode = blocking ? PIPE_WAIT : PIPE_NOWAIT;
	return SetNamedPipeHandleState(this->readPipe.getReader(), &mode, nullptr, nullptr);
}

bool PipedProcess::nonBlockingReturn() {
	auto errorCode = GetLastError();
	return errorCode == WAIT_IO_COMPLETION || errorCode == ERROR_NO_DATA;
}

void PipedProcess::startProcess() {
	assert(this->arguments.argc > 0 && this->arguments.argv != nullptr);
	/* This cannot be called anymore, because the arguments are freed later. */
	this->printSpawning();

	PROCESS_INFORMATION procInfo; 
	STARTUPINFO startInfo;

	ZeroMemory(&procInfo, sizeof(PROCESS_INFORMATION));
	ZeroMemory(&startInfo, sizeof(STARTUPINFO));
	startInfo.cb = sizeof(STARTUPINFO); 
	startInfo.hStdOutput = this->readPipe.getWriter();
	startInfo.hStdInput = this->writePipe.getReader();
	startInfo.dwFlags |= STARTF_USESTDHANDLES;

	/* Disable inheritance of these handles on the new process */
	assert(SetHandleInformation(this->readPipe.getReader(), HANDLE_FLAG_INHERIT, 0));
	assert(SetHandleInformation(this->writePipe.getWriter(), HANDLE_FLAG_INHERIT, 0));

	assert(CreateProcess(nullptr, this->arguments.argv[0], nullptr, nullptr, true, 0, nullptr, nullptr, &startInfo,
		&procInfo));

	/* Now that the new process is created, close unused handles */
	assert(AssignProcessToJobObject(PipedProcess::handlerJob, procInfo.hProcess));

	this->arguments.argv = nullptr;
	this->arguments.argc = 0;
	this->started = true;
	this->startCommunication();
}

#endif /* _WIN32 */