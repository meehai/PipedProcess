#ifdef _WIN32
#include <PipedProcess/Pipe.hpp>

Pipe::Pipe() {
	this->securityAttributes.nLength = sizeof(SECURITY_ATTRIBUTES);
	this->securityAttributes.bInheritHandle = true;
	this->securityAttributes.lpSecurityDescriptor = nullptr;

	assert(CreatePipe(&this->pipe[Pipe::Read], &this->pipe[Pipe::Write], &this->securityAttributes, 0));
}

bool Pipe::closePipe() const noexcept {
	return CloseHandle(this->pipe[Pipe::Read]) && CloseHandle(this->pipe[Pipe::Write]);
}

bool Pipe::closeReader() const noexcept {
	return CloseHandle(this->pipe[Pipe::Read]);
}

bool Pipe::closeWriter() const noexcept {
	return CloseHandle(this->pipe[Pipe::Write]);
}

int Pipe::write(const char *buffer, int size) const {
	DWORD written = -1;
	bool result = false;

	result = WriteFile(this->pipe[Pipe::Write], buffer, size, &written, nullptr);
	if(!result)
		return -1;

	return written;
}

int Pipe::read(char *buffer, int size) const {
	DWORD read = 0;
	bool result;

	while(read == 0) {
		result = ReadFile(this->pipe[Pipe::Read], buffer, size, &read, nullptr);
		if(!result)
			return -1;
	}

	if(read > 0)
		buffer[read] = '\0';
	return read;
}

#endif /* _WIN32 */