#include <assert.h>
#include <iostream>
#include <PipedProcess/PipedProcess.hpp>

using namespace std::literals::chrono_literals;

int main(int argc, char **argv) {
	assert(argc == 2 && "Usage: ./PipedProcess-Test <path-to-child-proces>");
	char *child_argv[] = {strdup(argv[1]), 0};
	std::string word;
	int res = -1;

	PipedProcess child;
	child.setArguments(1, child_argv);
	child.startProcess();
	free(child_argv[1]);

	word.reserve(1024);
	res = child.read(word, 1024);
	assert(res > 0 && word == "Message1");
	child.write("OK");
	fprintf(stderr, "Message 1 passed\n");

	res = child.read(word, 1024, PipedProcess::ReadFlags::Timer);
	assert(res > 0 && word == "Message2");
	child.write("OK");
	fprintf(stderr, "Message 2 passed\n");

	res = child.read(word, 1024, PipedProcess::ReadFlags::Timer, 500ms, 100ms);
	assert(res == -1);
	child.write("OK");
	fprintf(stderr, "Message 3 passed\n");

	word = "";
	try {
		res = child.read(word, 1024, PipedProcess::ReadFlags::Timer | PipedProcess::ReadFlags::ThrowExceptions);
	}
	catch (piped_process_exception &e) {
		assert(0 && "This should not throw exception");
	}
	assert(res > 0 && word == "Message4");
	child.write("OK");
	fprintf(stderr, "Message 4 passed\n");

    /* Out of time error */
	try {
		res = child.read(word, 1024, PipedProcess::ReadFlags::Timer | PipedProcess::ReadFlags::ThrowExceptions, 500ms,
			100ms);
		assert(0);
	} 
	catch (out_of_time &e) {
		child.write("OK");
		fprintf(stderr, "Message 5 passed\n");
	}

    /* Read nonblocking, should throw an exception for now. */
    try {
        res = child.read(word, 1024, PipedProcess::ReadFlags::NonBlocking | PipedProcess::ReadFlags::ThrowExceptions);
        assert(0);
    }
    catch (piped_process_exception &e) {
        fprintf(stderr, "Invalid flags test passed\n");
    }

	return 0;
}

